<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesiones', function (Blueprint $table) {
            $table->bigIncrements('id_profesion'); # Llave Primaria
            $table->string('nombre',50); # nombre Varchar
            $table->string('codigo',30); # codigo Varchar
            $table->boolean('estado')->default(true);

            $table->timestamps(); # create_at / update_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesiones');
    }
}
