<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    //por defecto laravel usa id
    protected $primaryKey="id_profesion";
    //por defecto laravel usar el plural de su modelo  (profesions)
    protected $table="profesiones";

    protected $fillable=[
        'nombre',
        'codigo',
        'estado',
    ];
}
