<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Profesion;
use App\Http\Requests\ProfesionRequest;

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ProfesionController extends Controller
{
    public function index(){
        // select *from profesines;
        $profesiones = Profesion::get(); // ORM
        //dd($profesiones);//print_r   exit

        // foreach($profesiones as $profesion){
        //     dump($profesion->nombre, $profesion->codigo);
        // }

        // dd($profesiones);

        return view('profesion.index',[
            'profesiones'=>$profesiones,
        ]);
    }

    public function frontendProfesion(){
        $curso = "CURSO DE LARAVEL";

        $profesiones = Profesion::get(); // ORM
        // dd($profesiones);
        return view('index',[
            'curso' => $curso,
            'pfs' => listProfesion()
        ]);
    }

    public function listadoProfesion(){

        return [
                'profesiones' => listProfesion()
        ];
    }

    //Cargar vista formulario crear
    public function create(){
        return view('profesion.create');
    }
    //Procesa un el formulario con post
    public function createStore(ProfesionRequest $request){
        $datos = $request->validationData();
        $profesion = Profesion::create($datos);
        return redirect()->route('profesion.index');
    }

    //Cargar vista formulario editar
    public function edit($id_profesion){
        $profesion = Profesion::find($id_profesion);
        return view('profesion.edit',[
            'profesion'=>$profesion
        ]);
    }
    //Procesa un el formulario con put o post
    public function editStore(ProfesionRequest $request){
    //public function editStore(){
        //$datos=request()->all();
        //dd($datos);
        //validaciones
        $datos=$request->validationData();

        $id_profesion=request()->input('id_profesion');
        //dd($datos,$id_profesion);

        $profesion=Profesion::find($id_profesion);
        $profesion->update($datos);
        return redirect()->route('profesion.index');
    }

    public function delete($id_profesion){
        $profesion=Profesion::find($id_profesion);
        $profesion->delete();

        return redirect()->route('profesion.index');
    }

    public function reporte(){
        $profesiones=Profesion::get();
        $html2pdf=new HTML2PDF('P','LEGAL','es',true,'UTF-8',array(15,20,15,20));
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML(view('profesion.reporte',['profesiones'=>$profesiones]));
        $html2pdf->output();
    }
    public function reporteExcel(){
        $profesiones=Profesion::all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $n=2;

        $sheet->getCell('A1')->setValue("Profesion");
        $sheet->getCell('B1')->setValue("Codigo");

        foreach($profesiones as $prof){
            $sheet->getCell('A'.$n)->setValue($prof->nombre);
            $sheet->getCell('B'.$n)->setValue($prof->codigo);
            $n=$n+1;
        }
        $spreadsheet->getActiveSheet()->setTitle('hojareporte1');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //make it an attachment so we can define filename
        header('Content-Disposition: attachment;filename="profesionesrep'.'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
