@extends('layouts.dashboard')


@section('content')

    <section class="content">
        <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                <h1>Profesiones</h1>
                <a href="{{ route('profesion.create') }}" class="btn btn-primary">CREAR</a>
                <a href="{{ route('profesion.create') }}" class="btn btn-secondary">CREAR</a>
                <a target="_blank" href="{{ route('profesion.reporte') }}" class="btn btn-secondary">Reporte</a>
                <a target="_blank" href="{{ route('profesion.reporteexcel') }}" class="btn btn-secondary">Reporte Excel</a>
                <br>
            </div>
        </div>
        </div>
    </section>



  <section class="content">
    <div class="container-fluid">

      <!-- /.row -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Responsive Hover Table</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>Profesion</th>
                    <th></th>
                  </tr>
                </thead>

                @foreach($profesiones as $profesion)
                <tr>
                    <td>
                        {{ $profesion->nombre }}
                    </td>
                    <td>
                        <a href="{{ route('profesion.edit',['id_profesion'=>$profesion->id_profesion]) }}" class="btn btn-primary">EDITAR</a>
                    </td>
                    <td>
                        <a href="{{ route('profesion.edit',[$profesion->id_profesion]) }}" class="btn btn-primary">EDITAR 2</a>
                    </td>
                    <td>
                        <a href="{{ route('profesion.delete',[$profesion->id_profesion]) }}" class="btn btn-danger">ELIMINAR</a>
                    </td>
                </tr>
                @endforeach

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>

    </div><!-- /.container-fluid -->
  </section>
@endsection
