@foreach($errors->all() as $error)
        {{ $error }}
@endforeach
EDITAR
<form name="form1" method="POST" action="{{ url('profesion/edit/store') }}">
    @csrf
    {{ method_field('PUT') }}
    <input type="hidden" name="id_profesion" value="{{ $profesion->id_profesion }}">
    Profesion: <input type="text" name="nombre" value="{{ old('nombre',$profesion->nombre) }}">
    <input type="submit" value="Guardar">
</form>