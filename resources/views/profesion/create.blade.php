@extends('layouts.dashboard')




@section('content')

    <section class="content">
        <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
                @foreach($errors->all() as $error)
                    {{ $error }}
                @endforeach
                <h1>CREAR NUEVA PROFESION</h1>
            </div>
        </div>
        </div>
    </section>



  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">CREAR NUEVA PROFESION</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form name="form1" method="POST" action="{{ route('profesion-store') }}">
                @csrf
               <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Profesion:</label>
                  <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required>

                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Codigo:</label>
                  <input type="text" class="form-control" name="codigo" value="{{ old('codigo') }}" required>
                </div>


              </div>
              <!-- /.card-body -->

              <div class="card-footer">

                <input type="submit" class="btn btn-primary" value="Guardar">
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection
