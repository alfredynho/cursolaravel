<link rel="stylesheet" href="{{ asset('frontend/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/cubeportfolio.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/jquery.fancybox.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/rs-plugin/css/settings.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/rs-plugin/css/navigation.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">

<style>
    /* Scroll Personalizado */
    body::-webkit-scrollbar {
    width: 10px;
    background: #ffffff;
   }
   body::-webkit-scrollbar-thumb {
    background: rgb(131, 84, 197);
    border-radius: 7px;
    border-right: 2px solid #f6f6f7;
   }
   /* Selccion de Texto Con Fondo */
   ::-moz-selection {
      color: #fff;
      background: #8354C5;
   }
   ::selection {
      color: #fff;
      background: #8354C5;
   }

</style>
