<div class="inner-wrapper">
    <span class="btn-close link" id="btn_sideNavClose"></span>
    <nav class="side-nav w-100">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link link scroll" href="#home">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#about">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#team">Team</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#portfolio">Work</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#price">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#blog">Blog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link scroll" href="#contact">Contact</a>
            </li>
        </ul>
    </nav>

    <div class="side-footer text-white w-100">
        <ul class="social-icons-simple">
            <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
            <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>
            <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
        </ul>
        <p class="text-white">&copy; 2019 Wexim. Made With Love by Themesindustry</p>
    </div>
</div>
