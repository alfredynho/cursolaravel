<div class="container">
    <a href="javascript:void(0)" title="Logo" class="logo link scroll">
        <!--Logo Default-->
        <img src="{{ asset('frontend/images/logo-white.png') }}" alt="logo" class="logo-dark default">
        {{-- asset = /home/usuario/laravel/portal/public/
                    frontend/images/logo-white.png --}}
    </a>

    <!--Nav Links-->
    <div class="collapse navbar-collapse" id="wexim">
        <div class="navbar-nav ml-auto">
                <a class="nav-link link scroll" href="#home">INICIO</a>
                <a class="nav-link link scroll" href="#about">INFO</a>
                <a class="nav-link link scroll" href="#team">EQUIPO</a>
                <a class="nav-link link scroll" href="#portfolio">TRABAJO</a>
                <a class="nav-link link scroll" href="#price">Pricing</a>
                <a class="nav-link link scroll" href="#blog">Blog</a>
                <a class="nav-link link scroll" href="#contact">Contact</a>
            <span class="menu-line"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
        </div>
    </div>

    <!--Side Menu Button-->
    <a href="javascript:void(0)" class="d-inline-block parallax-btn sidemenu_btn" id="sidemenu_toggle">
        <div class="animated-wrap sidemenu_btn_inner">
        <div class="animated-element">
                <span></span>
                <span></span>
                <span></span>
        </div>
        </div>
    </a>

</div>
