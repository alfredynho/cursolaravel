<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#8354C5">
    <title>Portal</title>
    <link rel="icon" href="images/favicon.ico">
    @include('frontend.statics.css')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--Loader Start-->
<div class="loader">
    @include('frontend.includes.loader')
</div>
<!--Loader End-->

<!--Header Start-->
<header class="cursor-light">

    <!--Navigation-->
    <nav class="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon">
        @include('frontend.includes.nav')
    </nav>

    <!--Side Nav-->
    <div class="side-menu">
        @include('frontend.includes.side')
    </div>
    <a id="close_side_menu" href="javascript:void(0);"></a>
    <!-- End side menu -->

</header>
<!--Header end-->

<!--slider-->
<section id="home" class="cursor-light p-0">
     @include('frontend.includes.slider')
</section>
<!--slider end-->

    @yield('content')
    {{-- Hace referencia a las plantillas hijas que heredaran --}}

<!--Contact End-->

    @include('frontend.includes.footer')

<!--Scroll Top-->
<a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
<!--Scroll Top End-->

<!--Animated Cursor-->
<div id="aimated-cursor">
    <div id="cursor">
        <div id="cursor-loader"></div>
    </div>
</div>
    @include('frontend.statics.js')
</body>
</html>
