<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'adminlte'], function() {
    Route::get('profesion/index','ProfesionController@index')->name('profesion.index');
    Route::get('profesion/create','ProfesionController@create')->name('profesion.create');
    Route::post('profesion/create/store','ProfesionController@createStore')->name('profesion-store');
    Route::get('profesion/edit/{id_profesion}','ProfesionController@edit')->name('profesion.edit');
    Route::put('profesion/edit/store','ProfesionController@editStore')->name('profesion-edit');
    Route::get('profesion/delete/{id_profesion}','ProfesionController@delete')->name('profesion.delete');
    Route::get('profesion/reporte','ProfesionController@reporte')->name('profesion.reporte');
    Route::get('profesion/reporteexcel','ProfesionController@reporteExcel')->name('profesion.reporteexcel');
    Route::get('profesion/api','ProfesionController@listadoProfesion')->name('profesion-api');
});

Route::get('personal/index','Dashboard/PersonalController@index')->name('personal.index');
// Route::get('personal/create','PersonalController@create')->name('personal.create');
// Route::post('personal/create/store','PersonalController@createStore')->name('personal-store');
// Route::get('personal/edit/{id_personal}','PersonalController@edit')->name('personal.edit');
// Route::put('personal/edit/store','PersonalController@editStore')->name('personal-edit');
// Route::get('personal/delete/{id_personal}','PersonalController@delete')->name('personal.delete');
// Route::get('personal/reporte','PersonalController@reporte')->name('personal.reporte');
// Route::get('personal/reporteexcel','PersonalController@reporteExcel')->name('personal.reporteexcel');

// Route::get('personal/api','PersonalController@listadoPersonal')->name('personal-api');


// Route::get('usuario/index','usuarioController@index')->name('usuario.index');
// Route::get('usuario/create','usuarioController@create')->name('usuario.create');
// Route::post('usuario/create/store','usuarioController@createStore')->name('usuario-store');
// Route::get('usuario/edit/{id_usuario}','usuarioController@edit')->name('usuario.edit');
// Route::put('usuario/edit/store','usuarioController@editStore')->name('usuario-edit');
// Route::get('usuario/delete/{id_usuario}','usuarioController@delete')->name('usuario.delete');
// Route::get('usuario/reporte','usuarioController@reporte')->name('usuario.reporte');
// Route::get('usuario/reporteexcel','usuarioController@reporteExcel')->name('usuario.reporteexcel');

// Route::get('usuario/api','usuarioController@listadousuario')->name('usuario-api');
