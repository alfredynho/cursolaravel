<?php
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('layouts.frontend');
// });

Route::get('/', 'ProfesionController@frontendProfesion')->name('frontend-index');

//Ruta con parametro opcional
Route::get('/saludo/{nombre?}', function ($nombre=null) {
    if($nombre){
        return "Hola como estas $nombre";
    } else {
        return "Hola desconocido";
    }
});

Route::get('/usuarios', function () {
    return "Lista de usuarios";
});

Route::get('/usuario/{id_usuario}', function ($id_usuario) {
    return "Detalle del usuario".$id_usuario;
})->where('id_usuario','[0-9]+');
